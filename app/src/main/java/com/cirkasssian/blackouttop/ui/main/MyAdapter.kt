package com.cirkasssian.blackouttop.ui.main

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cirkasssian.blackouttop.R
import com.cirkasssian.blackouttop.ui.main.BlackoutTopEffect.BLACKOUT_TRANSPARENT
import com.cirkasssian.blackouttop.ui.main.BlackoutTopEffect.calculateBlackoutTopElement

/**
 * Адаптер для отображения списка
 *
 * @param context для работы с системными компонентами
 */
class MyAdapter(context: Context) : RecyclerView.Adapter<MyHolder>() {

    /**
     * Список элементов которые необходимо отобразить
     */
    var items: List<MyModel> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    /**
     * Скролллистнер для API >= 23
     */
    private val onScrollChangeListener = View.OnScrollChangeListener { v, _, _, _, _ ->
        (v as? RecyclerView)?.calculateBlackoutTopElement()
    }

    /**
     * Скролллистнер для API < 23
     */
    private val onScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(rv: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(rv, dx, dy)
            rv.calculateBlackoutTopElement()
        }
    }

    private val inflater = LayoutInflater.from(context)

    override fun onAttachedToRecyclerView(rv: RecyclerView) {
        super.onAttachedToRecyclerView(rv)
        rv.apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                setOnScrollChangeListener(onScrollChangeListener)
            else
                setOnScrollListener(onScrollListener)
        }
    }

    override fun onDetachedFromRecyclerView(rv: RecyclerView) {
        super.onDetachedFromRecyclerView(rv)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            rv.removeOnScrollListener(onScrollListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MyHolder(inflater.inflate(R.layout.item_layout, parent, false))

    override fun getItemCount(): Int =
        items.size

    override fun onBindViewHolder(holder: MyHolder, position: Int) =
        holder.bind(items[position])

    /**
     * Обязательно нужно сбросить прозрачность блекаута, перед ПЕРЕИСПОЛЬЗОВАНИЕМ холдера
     */
    override fun onViewRecycled(holder: MyHolder) {
        super.onViewRecycled(holder)
        holder.setBlackoutAlpha(BLACKOUT_TRANSPARENT)
    }
}