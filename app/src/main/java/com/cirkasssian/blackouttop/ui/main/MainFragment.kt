package com.cirkasssian.blackouttop.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cirkasssian.blackouttop.R
import com.cirkasssian.blackouttop.databinding.FragmentMainBinding
import com.cirkasssian.blackouttop.utils.viewLifecycleAware
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * Экран для отображения списка
 */
class MainFragment : Fragment(R.layout.fragment_main) {

    private val binding by viewLifecycleAware {
        FragmentMainBinding.bind(requireView())
    }

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(this).get(MainViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = MyAdapter(requireContext())
        binding.list.adapter = adapter
        lifecycleScope.launch {
            viewModel.models.collect {
                adapter.items = it
            }
        }
    }

    companion object {

        /**
         * Создать новый инстанс фрагмента
         */
        fun newInstance() = MainFragment()
    }
}