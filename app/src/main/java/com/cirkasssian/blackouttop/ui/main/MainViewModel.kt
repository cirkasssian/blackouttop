package com.cirkasssian.blackouttop.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

/**
 * Вьюмодель для экрана отображения списка
 */
class MainViewModel : ViewModel() {

    private val mutableModels = MutableSharedFlow<List<MyModel>>()

    /**
     * Данные для отображения в списке
     */
    val models: SharedFlow<List<MyModel>> = mutableModels

    init {
        fillList()
    }

    private fun fillList() {
        viewModelScope.launch {
            flow {
                emit(createModels())
            }.collect(mutableModels::emit)
        }
    }

    private fun createModels() = listOf(
        MyModel("qwerty", "asdfgg"),
        MyModel("fdgfdg", "asdfgg"),
        MyModel("qwejhoihrty", "asdfgg"),
        MyModel("nczvnjkdls", "asdfgg"),
        MyModel("uroitu", "asdfgg"),
        MyModel("vnxcvklj", "asdfgg"),
        MyModel("hfrfskm", "asdfgg"),
        MyModel("kdjfshfiu", "asdfgg"),
        MyModel("ijpfjkdfjkn", "asdfgg"),
        MyModel("mnzclndslkvn", "asdfgg"),
        MyModel("elwjrlenfjd", "asdfgg"),
        MyModel("dfmsdfn;kj", "asdfgg"),
        MyModel("kflmsd'fpk", "asdfgg"),
        MyModel("xgasvfut", "asdfgg"),
        MyModel("kdjfshfiu", "asdfgg"),
        MyModel("ijpfjkdfjkn", "asdfgg"),
        MyModel("mnzclndslkvn", "asdfgg"),
        MyModel("elwjrlenfjd", "asdfgg"),
        MyModel("dfmsdfn;kj", "asdfgg"),
        MyModel("kflmsd'fpk", "asdfgg"),
        MyModel("xgasvfut", "asdfgg"),
        MyModel("kdjfshfiu", "asdfgg"),
        MyModel("ijpfjkdfjkn", "asdfgg"),
        MyModel("mnzclndslkvn", "asdfgg"),
        MyModel("elwjrlenfjd", "asdfgg"),
        MyModel("dfmsdfn;kj", "asdfgg"),
        MyModel("kflmsd'fpk", "asdfgg"),
        MyModel("xgasvfut", "asdfgg")
    )
}