package com.cirkasssian.blackouttop.ui.main

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.abs

/**
 * Содержит логику реализации эффекта затемнения верхнего элемента
 */
object BlackoutTopEffect {

    /**
     * Значение для сброса затемнения
     */
    const val BLACKOUT_TRANSPARENT = 0f

    /**
     * Вычисление значения затемнения верхнего элемента
     */
    fun RecyclerView.calculateBlackoutTopElement() {
        (layoutManager as? LinearLayoutManager)?.apply {
            (findViewHolderForAdapterPosition(findFirstVisibleItemPosition()) as? MyHolder)?.apply {
                getBlackoutAlpha(itemView.top, itemView.height)
                    .let(::setBlackoutAlpha)
            }
            (findFirstCompletelyVisibleItemPosition()..findLastCompletelyVisibleItemPosition()).forEach {
                (findViewHolderForAdapterPosition(it) as? MyHolder)
                    ?.setBlackoutAlpha(BLACKOUT_TRANSPARENT)
            }
        }
    }

    /**
     * Получить значение затемнения
     *
     * @param top координаты верхней границы элемента по шкале Y в px
     * @param height высота элемента списка в px
     */
    private fun getBlackoutAlpha(top: Int, height: Int) = if (top < 0) {
        abs(top).toFloat() / height
    } else {
        BLACKOUT_TRANSPARENT
    }
}