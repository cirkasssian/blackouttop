package com.cirkasssian.blackouttop.ui.main

/**
 * Модель с данными элемента списка
 *
 * @property title заголовок элемента списка
 * @property subTitle подзаголовок элемента списка
 */
data class MyModel(
    val title: String?,
    val subTitle: String?
)