package com.cirkasssian.blackouttop.ui.main

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.cirkasssian.blackouttop.databinding.ItemLayoutBinding

/**
 * Вьюхолдер элемента списка
 *
 * @param view корневой элемент разметки
 */
class MyHolder(view: View) : RecyclerView.ViewHolder(view) {

    /**
     * Биндинг макета
     */
    private val binding = ItemLayoutBinding.bind(view)

    /**
     * Привязка данных к представлению
     *
     * @param model модель с данными  для отображения
     */
    fun bind(model: MyModel) {
        binding.apply {
            model.title?.let(title::setText)
            model.subTitle?.let(subTitle::setText)
        }
    }

    /**
     * Установить затемнение для элемента списка
     *
     * @param alpha значение прозрачности
     */
    fun setBlackoutAlpha(alpha: Float) {
        binding.blackout.alpha = alpha
    }
}