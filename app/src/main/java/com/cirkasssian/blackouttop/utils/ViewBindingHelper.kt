package com.cirkasssian.blackouttop.utils

import androidx.fragment.app.Fragment
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

fun <T> Fragment.viewLifecycleAware(initialise: () -> T): ReadOnlyProperty<Fragment, T> =
    object : ReadOnlyProperty<Fragment, T>, DefaultLifecycleObserver {

        private var value: T? = null

        override fun onDestroy(owner: LifecycleOwner) {
            value = null
            this@viewLifecycleAware.lifecycle.removeObserver(this)
            super.onDestroy(owner)
        }

        override fun getValue(thisRef: Fragment, property: KProperty<*>): T =
            value
                ?: initialise().also {
                    value = it
                    this@viewLifecycleAware.viewLifecycleOwner.lifecycle.addObserver(this)
                }
    }